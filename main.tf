provider "aws" {
  region     = "us-east-1"
  skip_credentials_validation = true
  skip_requesting_account_id  = true
  skip_metadata_api_check     = true
  access_key = "AWS_ACCESS_ID"
  secret_key = "AWS_SECRET_ACCESS_KEY"
}

resource "aws_s3_bucket" "MyTest_S3" {
bucket = "TestPipeLine-01"
acl = "log-delivery-write"
force_destroy = true
	tags = {
Name = "MyTest_Log_Bucket"
	}
}

resource "aws_s3_bucket" "ep_www_site" {
bucket = "TestPipeLine"

logging {
target_bucket = "${aws_s3_bucket.MyTest_S3.bucket}"
target_prefix = "TestPipeLine"
	}
}
